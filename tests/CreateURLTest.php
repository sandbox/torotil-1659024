<?php

class CreateURLTest extends PHPUnit_Framework_TestCase {
	protected function setUp() {
		$this->ticket = array(
			'uid' => 12,
			'login_key' => 'realkeycode',
			'expire' => time()+200,
			'times' => 5,
			'path' => 'redirect/here',
		);
	}

	private function mockConf() {
		$config = $this->getMock('Conf', array('get'));
		$config->expects($this->any())->method('get')->will($this->returnValue(6));
		return $config;
	}

	private function mockedTicketSystem() {
		return $this->getMock('LoginTicketSystem', array('dbCreateTicket', 'dbFetchTicket'), array($this->mockConf(), NULL, NULL));
	}
	
	public function testCreateTicket() {
		$lts = $this->mockedTicketSystem();
		$t = $this->ticket;
		$ticket = $lts->createTicket($t['uid'], $t['expire'], $t['times'], $t['path']);
		$this->assertInternalType('object', $ticket);
	}

	public function testUrlFromTicket() {
		$lts = $this->mockedTicketSystem();
		$t = (object) $this->ticket;
		$lts->expects($this->once())->method('dbFetchTicket')->will($this->returnValue($t));
		$url = $lts->urlFromTicket($t);
		$this->assertRegExp("~/lt/$t->uid/[A-Za-z0-9\-_]+$~", $url);
	}

	public function testUrlFromTicket_length() {
		$lts = $this->mockedTicketSystem();
		$t = (object) $this->ticket;
		$lts->expects($this->once())->method('dbFetchTicket')->will($this->returnValue($t));
		$url = $lts->urlFromTicket($t);
		$this->assertRegExp("~/lt/$t->uid/[A-Za-z0-9\-_]{6}$~", $url);
	}
	
	public function testUrlFromTicket_collision() {
		$lts = $this->mockedTicketSystem();
		$t = (object) $this->ticket;
		$t2 = (object) (array('login_key' => 'realkeother') + $this->ticket);
		$lts->expects($this->at(0))->method('dbFetchTicket')->will($this->returnValue($t2));
		$lts->expects($this->at(1))->method('dbFetchTicket')->will($this->returnValue($t));
		$url = $lts->urlFromTicket($t);
		$this->assertRegExp("~/lt/$t->uid/[A-Za-z0-9\-_]{8}$~", $url);
	}

}