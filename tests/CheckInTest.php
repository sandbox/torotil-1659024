<?php

class CheckInTest extends PHPUnit_Framework_TestCase {
	protected function setUp() {
		$this->ticket = array(
			'uid' => 12,
			'login_key' => 'realkeycode',
			'expire' => time()+200,
			'times' => 5,
			'path' => 'redirect/here',
		);
	}

	private function mockConf() {
		$config = $this->getMock('Conf', array('get'));
		$config->expects($this->any())->method('get')->will($this->returnValue(6));
		return $config;
	}
	private function mockFetch($mocked, $ticket, $decrease = True) {
		$mocked->expects($this->any())->method('dbFetchTicket')->will($this->returnValue((object) $ticket));
		$mocked->expects($decrease ? $this->once() : $this->never())->method('dbDecreaseTimes');
	}
	private function mockRequest($good = TRUE) {
		$request = $this->getMock('Request', array('access_denied', 'go_to', 'finalize_login'));
		$request->expects($good ? $this->once() : $this->never())->method('finalize_login');
		$request->expects($good ? $this->never() : $this->once())->method('access_denied');
		return $request;
	}
	private function mockedTicketSystem($request, $user = NULL) {
		return $this->getMock('LoginTicketSystem', array('dbFetchTicket', 'dbDecreaseTimes'), array($this->mockConf(), null, $request, $this->mockSession($user)));
	}
	private function mockSession($user) {
		$session = $this->getMock('Session', array('getUser'));
		$session->expects($this->any())->method('getUser')->will($this->returnValue($user));
		return $session;
	}
	
	public function testCheckIn_GoodWithTimes() {
		$mocked = $this->mockedTicketSystem($this->mockRequest());
		$this->mockFetch($mocked, $this->ticket);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}

	public function testCheckIn_Good() {
		$mocked = $this->mockedTicketSystem($this->mockRequest());
		$this->mockFetch($mocked, array('times' => NULL) + $this->ticket, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}

	public function testCheckIn_ExpiredDate() {
		$mocked = $this->mockedTicketSystem($this->mockRequest(FALSE));
		$this->mockFetch($mocked, array('expire' => 0) + $this->ticket, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}

	public function testCheckIn_ExpiredTimes() {
		$mocked = $this->mockedTicketSystem($this->mockRequest(FALSE));
		$this->mockFetch($mocked, array('times' => 0) + $this->ticket, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}
	
	public function testCheckIn_NoKey() {
		$mocked = $this->mockedTicketSystem($this->mockRequest(FALSE));
		$this->mockFetch($mocked, NULL, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}

	public function testCheckIn_ShortKey() {
		$mocked = $this->mockedTicketSystem($this->mockRequest(FALSE));
		$this->mockFetch($mocked, $this->ticket, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'real');
	}

	public function testCheckIn_expiredButSession() {
		$request = $this->getMock('Request', array('access_denied', 'go_to', 'finalize_login'));
		$request->expects($this->never())->method('finalize_login');
		$request->expects($this->never())->method('access_denied');
		$user = (object) array('uid' => 12);
		$mocked = $this->mockedTicketSystem($request, $user);
		$this->mockFetch($mocked, array('times' => 0, 'expire' => 0) + $this->ticket, FALSE);
		$mocked->checkIn($user, 'realkeycode');
	}
	
	public function testCheckIn_ShortKeyButSession() {
		$request = $this->mockRequest(FALSE);
		$user = (object) array('uid' => 12);
		$mocked = $this->mockedTicketSystem($request, $user);
		$this->mockFetch($mocked, $this->ticket, FALSE);
		$mocked->checkIn($user, 'real');
	}
	
	public function testCheckIn_WrongSession() {
		$request = $this->getMock('Request', array('access_denied', 'go_to', 'finalize_login'));
		$request->expects($this->never())->method('finalize_login');
		$request->expects($this->never())->method('access_denied');
		$request->expects($this->never())->method('go_to');
		$mocked = $this->mockedTicketSystem($request, (object) array('uid' => 14));
		$this->mockFetch($mocked, $this->ticket, FALSE);
		$mocked->checkIn((object) array('uid' => 12), 'realkeyc');
	}

}