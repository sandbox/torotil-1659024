<?php

/**
 * Implements hook_rules_action_info();
 */
function login_ticket_rules_action_info() {
	$info = array();
	$info['login_ticket_genlink'] = array(
		'label' => t('Create login link'),
		'group' => t('User'),
		'parameter' => array(
			'user' => array(
				'label' => t('User account'),
				'type' => 'user',
				'description' => t('Create the login link for this user.'),
				'restriction' => 'selector',
			),
			'expire' => array(
				'label' => t('Expire date'),
				'description' => t("It's only possible to login with this link until the specified point in time"),
				'type' => 'date',
			),
			'times' => array(
				'label' => t('Expire after X clicks'),
				'description' => t('The link can only be used the specified amount of times.'),
				'type' => 'integer',
				'optional' => TRUE,
				'default value' => NULL,
				'allow null' => TRUE,
			),
			'path' => array(
				'label' => t('Redirect path'),
				'description' => t('Redirect the user to this path after a successful login'),
				'type' => 'text',
				'optional' => TRUE,
			),
		),
		'named parameter' => TRUE,
		'provides' => array(
			'login_link' => array(
				'label' => t('Link-to Session URL'),
				'type' => 'text',
			),
		),
	);

	return $info;
}

/**
 * Rules action callback to create a new login link
 * @return string
 */
function login_ticket_genlink($args) {
	$args += array(
		'times' => NULL,
		'path' => '<front>',
	);
	$lts = LoginTicketSystem::instance();
	$ticket = $lts->createTicket($args['user']->uid, $args['expire'], $args['times'], $args['path']);
	$url = $lts->urlFromTicket($ticket);
	return array('login_link' => $url);
}
